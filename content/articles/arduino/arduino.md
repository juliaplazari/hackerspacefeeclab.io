Title: Arduino UNO
Date: 2020-08-08

## O que é?
O Arduino UNO é uma placa de desenvolvimento composta por um microcontrolador da Atmel (AVR ATMEGA328P) e outros circuitos de entrada/saída para facilitar seu uso. Ela pode ser facilmente conectada a um computador e programada via IDE (Integrated Development Environment, ou Ambiente de Desenvolvimento Integrado). Também é possível utilizar seu editor de texto preferido e compilar posteriormente. A linguagem utilizada por padrão se chama Arduino e é baseada em C/C++, mas também é possível programar diretamente em C. Tanto a linguagem quanto o hardware são abertos (FLOSS).

## História
A primeira placa de desenvolvimento Arduino foi criada em 2005 no Interaction Design Institute Ivrea (Instituto de Design de Interação de Ivrea), no norte da Itália por um grupo de 5 pesquisadores (Massimo Banzi, David Cuartielles, Tom Igoe, Gianluca Martino e David Melli) que tinha como intuito criar um dispositivo que fosse destinado a artistas, designers e qualquer um que tenha interesse em criar objetos ou ambientes interativos. E assim surgiu o Arduino, uma ferramenta funcional, fácil de programar e acessível economicamente.

## Especificações
1. Conector USB.
2. Fonte Externa (Conector Jack).
3. Pinos de referência, terra.
4. Conector de Alimentação de 5V.
5. Conector de Alimentação de 3,3V.
6. Entradas Analógicas.
7. Entradas/Saídas Digitais (GPIO's).
8. Saídas PWM.
9. AREF, usado para mudar o valor da tensão de referência.
10. Botão de Reset, usado para reiniciar o código carregado no Arduino.
11. Indicador LED de Energia.
12. TX e RX – LEDs de transmissão e recepção de dados.
13. Circuito Integrado ATmega328P.
14. Regulador de Tensão.

<br/>
<img style='display: block; align: center; margin-right: auto; margin-left:auto; max-width: 90%; height: auto;' alt='Attiny pinout' src='images/placa_arduino.png'/>

## Pinagem

<br/>
<a href="images/pin_arduino.png"><img style='display: block; align: center; margin-right: auto; margin-left:auto; max-width: 70% ; height: auto;' alt='Atmega328 pinout' src='images/pin_arduino.png'/></a>

## Links

[Site do arduino](https://www.arduino.cc/)

[Manual do Atmega328P](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)

[TED Talk de um dos criadores](https://www.ted.com/talks/massimo_banzi_how_arduino_is_open_sourcing_imagination?language=pt-br)
