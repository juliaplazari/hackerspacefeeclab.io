Title: documentos

## Criação

Todos os documentos também estão disponíveis em um [repositório do gitlab](https://gitlab.com/hackerspacefeec/documentos).

- [Proposta de criação do hackerspace](https://gitlab.com/hackerspacefeec/documentos/-/blob/master/proposta_hackerspace.pdf)

## Identidade visual (temporária)

Todos os documentos de identidade visual também estão disponíveis em um [repositório do gitlab](https://gitlab.com/hackerspacefeec/visual).

- [Logo horizontal](https://gitlab.com/hackerspacefeec/visual/-/blob/master/logo/logo_hackerspace1.png)
- [Logo vertical](https://gitlab.com/hackerspacefeec/visual/-/blob/master/logo/logo_hackerspace1-2.png)

